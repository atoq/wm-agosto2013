#xrandr --output VGA-0 --auto --primary --output DVI-0 --auto --right-of VGA-0 &
thunar --daemon &
nitrogen --restore &

mpd &
#[ ! -s ~/.mpd/pid ] && mpd &

setxkbmap -model pc104 -layout latam,ru &

x11vnc -nap -wait 50 -noxdamage -rfbauth ~/.vnc/passwd -display :0 -forever -bg

#dropboxd
