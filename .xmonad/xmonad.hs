--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--

-- Imports {{{

import XMonad
import System.IO
--import Data.Monoid
import System.Exit

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

--actions 
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect

--layouts
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
--hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageHelpers (isDialog, isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Layout.Fullscreen (fullscreenFull)

import Control.Monad (liftM2)

-- utils
import XMonad.Util.EZConfig
import XMonad.Util.Run

-- }}}


-- The basics {{{

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal      = "urxvt"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Width of the window border in pixels.
--
myBorderWidth   = 2

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod4Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["1:prin","2:web","3:web","4:deve","5:deve","6:elec","7:elec","8:mult","9:mult"]

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#292929"
myFocusedBorderColor = "#30bfa0"

-- }}}


-- Taskbar config {{{

myDzenPP h = defaultPP
    { ppCurrent         = dzenColor myFFGColor myFBGColor . wrap (" ^fg(" ++ myFFGColor ++ ")^i(" ++ myIconDir ++ "/xbm8x8/eye_l.xbm)" ++ " ^fg(" ++ myFFGColor ++ ")") " "
    , ppVisible         = dzenColor myVFGColor myVBGColor . wrap (" ^fg(" ++ myVFGColor ++ ")^i(" ++ myIconDir ++ "/xbm8x8/eye_r.xbm)" ++ " ^fg(" ++ myVFGColor ++ ")") " "
    , ppHidden          = dzenColor myDFGColor myDBGColor . wrap (" ^fg(" ++ myPPLCgrid ++ ")^i(" ++ myIconDir ++ "/dzen_bitmaps/has_win.xbm) ") " "
    , ppHiddenNoWindows = dzenColor myDFGColor myDBGColor . wrap (" ^i(" ++ myIconDir ++ "/dzen_bitmaps/has_win_nv.xbm) ") " "
    , ppUrgent          = dzenColor myUFGColor myUBGColor . wrap (" ^i(" ++ myIconDir ++ "/xbm8x8/info_03.xbm) ") " " . dzenStrip
    , ppWsSep           = ""
    , ppSep             = "  >>  "
    , ppLayout          = dzenColor myDFGColor myDBGColor .
                          (\x -> case x of
                          "Full"            -> "^fg(" ++ myPPLCfull ++ ")^i(" ++ myIconDir ++ "/dzen_bitmaps/full.xbm)"
                          "Spacing 0 Grid" -> "^fg(" ++ myPPLCgrid ++ ")^i(" ++ myIconDir ++ "/dzen_bitmaps/mtall.xbm)"
                          "Spacing 0 Tall" -> "^fg(" ++ myPPLCtall ++ ")^i(" ++ myIconDir ++ "/dzen_bitmaps/tall.xbm)"
                          _                 -> x
                          )
    , ppTitle           = (" " ++) . dzenColor myPPTitle myDBGColor . dzenEscape
    , ppOutput          = hPutStrLn h
    }

myDFGColor = "#ffffff" -- normal text color; white
myPPLCfull = "#f73149"
myPPLCgrid = "#31d3f7"
myPPLCtall = "#fafa87"
myPPTitle = "#fa8787"
myDBGColor = "#292929" -- normal bg color, black
myFFGColor = myDBGColor -- highlighted bg color, black
myFBGColor = "#55cf99" -- highlighted text color, green
myVFGColor = "#8abfb0"
myVBGColor = "#3b848c"
myUFGColor = "#4390B1"
myUBGColor = "#d91e0d"
myIFGColor = "#8abfd0"
myIBGColor = myDBGColor
mySColor   = myDFGColor

--myStatusBar = "dzen2 -x '0' -y '0' -h '20' -w '1150' -ta 'l' -bg '" ++ myDBGColor ++ "' -fn '" ++ myFont ++ "'"
myStatusBar = "dzen2 -x '0' -y '0' -h '20' -w '1650' -ta 'l' -bg '" ++ myDBGColor ++ "' -fn '" ++ myFont ++ "'"
-- myTrayBar   = "conky -c ~/.conky_top    | dzen2 -x '1700' -y '0'    -h '20' -ta 'c' -bg '" ++ myDBGColor ++ "' -fg '" ++ myDFGColor ++ "' -fn '" ++ myFont ++ "'"
myTrayBar   = "/home/david/.xmonad/traybarscript.zsh | dzen2 -x '1620' -y '0' -h '20' -w '336' -ta 'r' -bg '" ++ myDBGColor ++ "' -fg '" ++ myDFGColor ++ "' -fn '" ++ myFont ++ "'"
--myTrayBar   = "/home/david/.xmonad/traybarscript.zsh | dzen2 -x '1150' -y '0' -h '20' -ta 'r' -bg '" ++ myDBGColor ++ "' -fg '" ++ myDFGColor ++ "' -fn '" ++ myFont ++ "'"
--trayerPanel = "trayer --edge top --align right --SetDockType true --SetPartialStrut true --expand true --width 12 --transparent true --tint 0x292929 --height 20 --alpha 0"
--trayerPanel = "trayer -x '1340' -y '0' --SetDockType true --SetPartialStrut true --expand true --width 12 --transparent true --tint 0x292929 --height 20 --alpha 0"
myBottomBar = "conky -c ~/.conky_bottom | dzen2 -x '0'    -y '1060' -h '20' -ta 'c' -bg '" ++ myDBGColor ++ "' -fg '" ++ myDFGColor ++ "' -fn '" ++ myFont ++ "'"
myIconDir = "/home/david/.dzen/"

myFont = "-*-liberation mono-medium-r-normal-*-10-*-*-*-*-*-*-*"



-- }}}


-- key bindings {{{


------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    , ((modm,               xK_p     ), spawn "dmenu_run")

    -- launch gmrun
    , ((modm .|. shiftMask, xK_p     ), spawn "gmrun")

    -- set screensaver
    , ((modm .|. shiftMask, xK_z     ), spawn "xscreensaver-command -lock")

    -- print select window
    , ((modm,               xK_Print ), spawn "sleep 0.2; scrot -se 'mv $f ~/Multimedia/CaptureImage/'")

    -- print select window
    , ((modm,               xK_Print ), spawn "scrot -e 'mv $f ~/Multimedia/CaptureImage/'")

-- close focused window
    , ((modm .|. shiftMask, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_Return), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    --Programas
    
    , ((modm .|. shiftMask,      xK_w        ), spawn "chromium")
    , ((modm .|. shiftMask,      xK_g        ), spawn "gimp")
    , ((modm .|. shiftMask,      xK_t        ), spawn "thunar")
    , ((modm .|. shiftMask,      xK_f        ), spawn "firefox")
    , ((modm .|. shiftMask,      xK_o        ), spawn "opera")
--    , ((modm .|. shiftMask,      xK_e        ), spawn "eagle")
--    , ((modm .|. shiftMask,      xK_b        ), spawn "bluefish")
--    , ((modm .|. shiftMask,      xK_n        ), spawn "geany")
--    , ((modm .|. shiftMask,      xK_y        ), spawn "gnome-system-monitor")
--    , ((modm .|. shiftMask,      xK_s        ), spawn "skype")
--    , ((modm .|. shiftMask,      xK_l        ), spawn "simple-scan")
--    , ((modm .|. shiftMask,      xK_h        ), spawn "epiphany")
 

    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    -- ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    --[((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
    --    | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
    --    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

-- }}}

-- Mouse bindings {{{
--
------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

-- }}}

-- Hooks & Layouts {{{

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayoutHook = tiled ||| grid ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = spacing 0 $ Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

     grid = spacing 0 $ Grid

     full = noBorders $ Full	

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll . concat $  -- Quite el "(" antes de composeAll
--    [ [className =? "GnomeMPlayer"   --> doFloat]
--   , [className =? "Gimp"           --> doFloat]
--   , [className =? "Chromium"       --> doShift "2:web"]
--   , [className =? "Opera"          --> doShift "2:web"]
--   , [className =? "Thunar"         --> doShift "4:deve"]
--   , [resource  =? "desktop_window" --> doIgnore]
--   , [resource  =? "kdesktop"       --> doIgnore]
--   ])

    [   [isDialog --> doFloat]
    ,   [isFullscreen --> (doF W.focusDown <+> doFullFloat)]
    ,   [className =? c --> doCenterFloat | c <- myCFloats ]
    ,   [title =? t --> doCenterFloat | t <- myTFloats]
    , [title =? t --> doCenterFloat | t <- myTFloats]
    , [resource =? r --> doCenterFloat | r <- myRFloats]
    , [(className =? i <||> resource =? i) --> doIgnore | i <- myIgnores]
    , [ className =? "Screenkey" --> doIgnore]
--    , [className =? "Screenkey" --> placeHook/ fixed(0,1) ) <+> doFloat <+> doF W.focusDown]
    , [(className =? x <||> title =? x <||> resource =? x) --> doSink | x <- mySinks]
    , [(className =? x <||> title =? x <||> resource =? x) --> (doFullFloat <+> doCenterFloat) | x <- myFullscreens]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift "1:prin" | x <- my1Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift "2:web" | x <- my2Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift "3:web" | x <- my3Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift "4:deve" | x <- my4Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "5:deve" | x <- my5Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "6:elec" | x <- my6Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "7:elec" | x <- my7Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "8:mult" | x <- my8Shifts]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo "9:mult" | x <- my9Shifts]
    , [manageDocks]
    ]
--        [className =? i --> doFloat | i <- myClassFloats ]
--        , [ className =? i --> doFloat | i <- myClassGpic   ]
    
    where

    -- Hook used to shift windows without focusing them
    doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    -- Hook used to push floating windows back into the layout
    -- This is used for gimp windwos to force them into a layout.
    doSink = ask >>= \w -> liftX (reveal w) >> doF (W.sink w)
    -- Float dialogs, Download windows and Save dialogs
    myCFloats = ["Sysinfo", "XMessage"]
    myTFloats = ["Downloads", "Save As..."]
    myRFloats = ["Dialog","galculator"]
    -- Ignore gnome leftovers
    myIgnores = ["Unity-2d-panel", "Unity-2d-launcher", "desktop_window", "kdesktop"]
    mySinks = ["gimp"]
    -- Run VLC, firefox and VLC on fullscreen
    myFullscreens = ["vlc", "Image Viewer", "firefox", "opera"]
    -- Define default workspaces for some programs
    my1Shifts = ["Firefox-bin", "Firefox","chromium", "firefox", "Firefox Web Browser", "Opera"]
    my2Shifts = ["thunderbird", "Thunderbird-bin", "thunderbird-bin", "Thunderbird", "skype", "skype-wrapper", "Skype"]
    my3Shifts = ["quartus", "matlab"]
    my4Shifts = ["geany"]
    my5Shifts = ["eagle", "kicad", "oregano", "ktechlab", "klayout"]
    my6Shifts = ["hotot"]
    my7Shifts = ["ncmpcpp"]
    my8Shifts = ["gimp", "GIMP Image Editor"]
    my9Shifts = ["vlc", "thunar"]

--    myClassFloats = ["feh"]
--    myClassGpic = ["gimp"]
--	role	= stringProperty "WM_WINDOW_ROLE"
--	name	= stringProperty "WM_NAME"

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
--Esto estaba habilitado Originalmente  OOooOO

--myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
--Esto estaba habilitado Originalmente  OOooOO

--myStartupHook = return ()

-- }}}

-- Main {{{

------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
    programs <- spawnPipe "sh /home/david/.xmonad/autostart.sh"
    dzen <- spawnPipe myStatusBar
    tray <- spawnPipe myTrayBar
--    trayer <- spawnPipe trayerPanel
    bottom <- spawnPipe myBottomBar
    xmonad $ ewmh $ withUrgencyHook NoUrgencyHook defaultConfig {
       -- the basics
        terminal = myTerminal,
        focusFollowsMouse = myFocusFollowsMouse,
        borderWidth = myBorderWidth,
        modMask = myModMask,
        workspaces = myWorkspaces,
        normalBorderColor = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,  
       -- keybinds
        keys = myKeys,
        mouseBindings = myMouseBindings,
       -- hooks & layouts
        layoutHook = avoidStruts $ myLayoutHook,
        manageHook = manageHook defaultConfig <+> myManageHook, 
        logHook = myLogHook >> (dynamicLogWithPP $ myDzenPP dzen) >> setWMName "LG3D"
--        startupHook = ewmhDesktopsStartup >> setWMName "LG3D"
    }  
-- }}}
-- vim:foldmethod=marker sw=4 sts=4 ts=4 tw=0 et ai nowrap

