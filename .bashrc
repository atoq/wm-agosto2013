#
# ~/.bashrc
#

# If not running interactively, don't do anything
#[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\[\e[1;32m\][\u@\h \W]\$\[\e[0m\] '
#TERM='rxvt-unicode'
TERM='rxvt-unicode-256color'
COLORTERM='rxvt-unicode-256color'
#PS1='[\u@\h \W]\$ '
export EDITOR="vim" 

if [ ! $DISPLAY ] ; then
    if [ "$SSH_CLIENT" ] ; then
        export DISPLAY='echo $SSH_CLIENT|cut -f1 -d\ ':0.0
    fi
fi

source /usr/share/doc/pkgfile/command-not-found.bash
source /etc/profile.d/cnf.sh

#alias ls='ls --color=yes' #Te mostrara siempre la salida de ls con colores.
#alias grep='grep --color=auto' #Te mostrará la salida de grep con colores.
